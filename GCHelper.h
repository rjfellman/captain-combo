//
//  GCHelper.h
//  Captain Combo
//
//  Created by Russ Fellman on 3/24/14.
//  Copyright (c) 2014 Russ Fellman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GameKit/GameKit.h>

@interface GCHelper : NSObject <GKGameCenterControllerDelegate> {
    BOOL gameCenterAvailable;
    BOOL userAuthenticated;
}

@property (assign, readonly) BOOL gameCenterAvailable;

+ (GCHelper *)sharedInstance;
- (void)authenticateLocalUser;
- (void) showLeaderboard: (NSString*) leaderboardID inViewController:(UIViewController<GKGameCenterControllerDelegate>*)viewController;
- (void) reportScore: (int64_t) score forLeaderboardID: (NSString*) identifier;

@end