//
//  GameOverViewController.m
//  Chain
//
//  Created by Russ Fellman on 3/22/14.
//  Copyright (c) 2014 Russ Fellman. All rights reserved.
//

#import "GameOverViewController.h"
#import "Constants.h"
#import <Social/Social.h>

@interface GameOverViewController ()

@end

@implementation GameOverViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil score:(int)score delegate:(id<GameOverViewControllerDelegate>)delegate{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.score = score;
        self.delegate = delegate;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.scoreLabel.format = @"%d";
    self.scoreLabel.method = UILabelCountingMethodLinear;
    [self setupViewForScore];
}

-(NSString*)getScoreDescription{
    return @"";
}

-(BOOL)canDisplayBannerAds{
    return YES;
}

-(void)setupViewForScore{
    [self.scoreLabel countFrom:0 to:self.score];
    [self.scoreDescLabel setText:[self getScoreDescription]];
}

-(IBAction)shareOnFacebook:(id)sender{
    [Flurry logEvent:@"shared on Facebook"];
    SLComposeViewController *facebookComposer;
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        facebookComposer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [facebookComposer setInitialText:[NSString stringWithFormat:@"I got %i points playing %@. Think you can beat me? \n\n %@", self.score, ApplicationName, iTunesLink]];
        [self presentViewController:facebookComposer animated:YES completion:^{
            
        }];
    } else {
        [Flurry logEvent:@"shared on Facebook failed"];
        [self displayErrorMessage:@"Facebook"];
    }
}


-(IBAction)shareOnTwitter:(id)sender{
    [Flurry logEvent:@"shared on Twitter"];
    SLComposeViewController *twitterComposer;
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        twitterComposer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [twitterComposer setInitialText:[NSString stringWithFormat:@"I got %i points playing %@. Think you can beat me? \n\n %@",self.score, ApplicationName, iTunesLink]];
        [self presentViewController:twitterComposer animated:YES completion:^{
            
        }];
    } else {
        [Flurry logEvent:@"shared on Twitter failed"];
        [self displayErrorMessage:@"Twitter"];
    }
}

-(IBAction)showLeaderboard:(id)sender{
    [[GCHelper sharedInstance] showLeaderboard:@"CCBOARD99" inViewController:self];
}

-(void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)displayErrorMessage:(NSString*)service{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[NSString stringWithFormat:@"Please enable the %@ service in iOS Settings and try again.", service] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

-(IBAction)playAgain:(id)sender{
    [Flurry logEvent:@"played again"];
    [self.delegate willPlayAgain];
}

@end
