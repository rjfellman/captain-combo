//
//  MyScene.m
//  Chain
//
//  Created by Russ Fellman on 3/17/14.
//  Copyright (c) 2014 Russ Fellman. All rights reserved.
//

#import "MyScene.h"
#import "Constants.h"
#import "Entity.h"
#import "Flurry.h"

@implementation MyScene

SKLabelNode *title;
SKSpriteNode *guide;
SKSpriteNode *shooter;
SKLabelNode *counter;
SKLabelNode *scoreLabel;
SKSpriteNode *bg1, *bg2;
NSMutableArray *badGuys;
NSMutableArray *lasers;
NSMutableArray *explosions;
NSMutableArray *particleArrays;
NSMutableArray *weapons;
int score;
int count;
int combo;
BOOL gameInProgress;
BOOL gameEnded;
BOOL inCountdown;
UIColor *fontColor;

#pragma mark Initialization

-(id)initWithSize:(CGSize)size delegate:(id)delgate {
    if (self = [super initWithSize:size]) {
        /* Setup your scene here */
        
        self.backgroundColor = [SKColor clearColor];
        self.physicsWorld.contactDelegate = self;
        self.physicsWorld.gravity = CGVectorMake(0.0f, 0.0f);
        
        NSString *backdropName = BackDropFilename;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            backdropName = [backdropName stringByAppendingString:@"_ipad"];
        }
        
        bg1 = [SKSpriteNode spriteNodeWithImageNamed:backdropName];
        bg1.anchorPoint = CGPointZero;
        bg1.position = CGPointMake(0, 0);
        [self addChild:bg1];
        
        bg2 = [SKSpriteNode spriteNodeWithImageNamed:backdropName];
        bg2.anchorPoint = CGPointZero;
        bg2.position = CGPointMake(0, bg1.size.height-1);
        [self addChild:bg2];
        
        fontColor = [UIColor whiteColor];
        
        title = [SKLabelNode labelNodeWithFontNamed:DefaultFontName];
        
        [self initShooter];
        
        guide = [SKSpriteNode spriteNodeWithImageNamed:@"guide"];
        guide.position = shooter.position;
        [self addChild:guide];
        
        title.text = @"Captain Combo";
        title.name = @"title";
        title.fontSize = 30;
        title.fontColor = fontColor;
        title.position = CGPointMake(CGRectGetMidX(self.frame),
                                       CGRectGetMidY(self.frame));
        
        [self addChild:title];

        score = 0;
        badGuys = [NSMutableArray array];
        lasers = [NSMutableArray array];
        explosions = [NSMutableArray array];
        particleArrays = [NSMutableArray array];
        weapons = [NSMutableArray array];
        
        self.delegate = delgate;
    }
    return self;
}

- (void)initShooter{
    shooter = [SKSpriteNode spriteNodeWithImageNamed:ShooterFilename];
    shooter.position = CGPointMake(CGRectGetMidX(self.frame) , CGRectGetMinY(self.frame) + 100);
    shooter.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(shooter.frame.size.width/2, shooter.frame.size.height/2)];
    shooter.name = @"shooter";
    shooter.physicsBody.categoryBitMask = shooterCategory;
    shooter.physicsBody.collisionBitMask =  weaponCategory;
    shooter.physicsBody.contactTestBitMask =  weaponCategory;
    [self addChild:shooter];
    
    [Flurry logEvent:@"Started Game"];
}

#pragma mark ScoreCard

- (void)updateScoreWith:(int)scoreUpdate{
    scoreLabel.text = [NSString stringWithFormat:@"Score: %i", score];
}

- (void)createScoreBanner{
    scoreLabel = [SKLabelNode labelNodeWithFontNamed:DefaultFontName];
    scoreLabel.text = @"Score: 0";
    scoreLabel.name = @"score";
    scoreLabel.fontColor = fontColor;
    scoreLabel.fontSize = 15;
    scoreLabel.position = CGPointMake(CGRectGetMinX(self.frame) + 50, CGRectGetMaxY(self.frame) - 50);
    [self addChild:scoreLabel];
}

- (void)calculateScoreUpdate{
    score = score + (10*(score/100+1));
    [self updateScoreWith:0];
}

#pragma mark Game Flow

- (void)beginGame{
    [guide removeFromParent];
    [title removeFromParent];
    inCountdown = YES;
    [self countdownFrom:2];
}

- (void)countdownFrom: (int)countTimer {
    [counter removeFromParent];
    counter = [SKLabelNode labelNodeWithFontNamed:DefaultFontName];
    counter.name = @"counter";
    count = countTimer;
    counter.text = [NSString stringWithFormat:@"%i", count];
    counter.fontSize = 70;
    counter.fontColor = fontColor;
    counter.position = CGPointMake(CGRectGetMidX(self.frame),
                                   CGRectGetMidY(self.frame));
    
    SKAction *countAction = [SKAction scaleTo:0 duration:1];
    [counter runAction:[SKAction repeatAction:countAction count:1] completion:^{
        count--;
        if (count > 0) {
            [self countdownFrom:count];
        } else {
            [self countdownEnded];
        }
    }];
    [self addChild:counter];
}

- (void)displayLabelWithTitle:(NSString*)title{
    SKLabelNode *label = [SKLabelNode labelNodeWithFontNamed:DefaultFontName];
    label.text = title;
    label.fontSize = 40;
    label.fontColor = fontColor;
    if ([title isEqualToString:@"Combo Bonus!!"]) {
        label.fontColor = [UIColor orangeColor];
    }
    label.position = CGPointMake(CGRectGetMidX(self.frame),
                                 CGRectGetMidY(self.frame));
    
    SKAction *countAction = [SKAction scaleTo:0 duration:1.5];
    [label runAction:[SKAction repeatAction:countAction count:1] completion:^{
        [label removeFromParent];
        [self enterBadguys];
    }];
    [self addChild:label];
}

- (void)countdownEnded{
    gameInProgress = YES;
    inCountdown = NO;
    [self displayLabelWithTitle:@"Begin!"];
    [self createScoreBanner];
}

- (void)gameOver{
    SKLabelNode *gameOver = [SKLabelNode labelNodeWithFontNamed:DefaultFontName];
    gameOver.text = @"GAME OVER!";
    gameOver.fontColor = fontColor;
    gameOver.name = @"game_over";
    gameOver.fontSize = 10;
    gameOver.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    SKAction *gameOverIn = [SKAction scaleBy:3 duration:1];
    [gameOver runAction:gameOverIn completion:^{
        gameInProgress = NO;
        gameEnded = YES;
        [Flurry logEvent:@"Game Over with Score" withParameters:@{@"score":[NSNumber numberWithInt:score]}];
        [self.delegate gameEndedWithScore:score];
    }];
    [self addChild:gameOver];
}

#pragma mark Explosions

- (void)explosionAtLocation:(CGPoint)location withSpread:(BOOL)spread{
    SKSpriteNode *explosion = [SKSpriteNode spriteNodeWithImageNamed:ExplosionFilename];
    explosion.position = location;
    SKAction *explode = [SKAction scaleTo:4 duration:0.2];
    [explosion runAction:explode completion:^{
        if (spread) {
            NSArray *particles = [self getParticlesForExplosion:explosion];
            for (SKNode *particle in particles) {
                [self addChild:particle];
            }
            [particleArrays addObject:particles];
        }
       [self exploded];
    }];
    [explosions addObject:explosion];
    [self addChild:explosion];
}

- (void)exploded{
    [self removeChildrenInArray:explosions];
}

#pragma mark Particles

- (NSArray*)getParticlesForExplosion:(SKNode*)activeExplosion{
    NSMutableArray *particles = [NSMutableArray array];
    for (int i = 1; i <= 8; i++) {
        SKSpriteNode *particle = [SKSpriteNode spriteNodeWithColor:[UIColor greenColor] size:CGSizeMake(7, 7)];
        particle.position = activeExplosion.position;
        particle.name = [NSString stringWithFormat:@"particle"];
        SKAction *particleAction = [SKAction moveBy:[self getVectorForParticle:i] duration:0.5];
        particle.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:particle.size];
        particle.physicsBody.categoryBitMask = particleCategory;
        particle.physicsBody.collisionBitMask = baddieCategory;
        particle.physicsBody.contactTestBitMask = baddieCategory;
        [particle runAction:particleAction completion:^{
            [[particleArrays firstObject] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                [obj removeFromParent];
            }];
            [particleArrays removeObject:[particleArrays firstObject]];
        }];
        [particles addObject:particle];
    }
    return particles;
}

-(CGVector)getVectorForParticle:(int)particleNumber{
    CGVector vector;
    switch (particleNumber) {
        case 1:
            vector = CGVectorMake(50, 50);
            break;
        case 2:
            vector = CGVectorMake(50, -50);
            break;
        case 3:
            vector = CGVectorMake(-50, 50);
            break;
        case 4:
            vector = CGVectorMake(-50, -50);
            break;
        case 5:
            vector = CGVectorMake(0, 50);
            break;
        case 6:
            vector = CGVectorMake(50, 0);
            break;
        case 7:
            vector = CGVectorMake(0, -50);
            break;
        case 8:
            vector = CGVectorMake(-50, 0);
            
        default:
            break;
    }
    return vector;
}


#pragma mark Badguys

- (void)enterBadguys{
    [self newBadguy];
    
}

- (NSDictionary*)getBadguyTrajectory{
    NSMutableDictionary *trajectory = [[NSMutableDictionary alloc] init];
    CGPoint startingPoint;
    CGPoint endingPoint;
    //come in from the left or right
    if (arc4random() %2 + 1 == 2) {
        startingPoint = CGPointMake(CGRectGetMaxX(self.frame), arc4random() %370 + 100);
        [trajectory setObject:[NSNumber numberWithInt:startingPoint.x] forKey:@"startingPointX"];
        [trajectory setObject:[NSNumber numberWithInt:startingPoint.y] forKey:@"startingPointY"];
        
        endingPoint = CGPointMake(CGRectGetMinX(self.frame), arc4random()%370 + 100);
        [trajectory setObject:[NSNumber numberWithInt:endingPoint.x] forKey:@"endingPointX"];
        [trajectory setObject:[NSNumber numberWithInt:endingPoint.y] forKey:@"endingPointY"];
    } else {
        startingPoint = CGPointMake(CGRectGetMinX(self.frame), arc4random() %370 + 100);
        [trajectory setObject:[NSNumber numberWithInt:startingPoint.x] forKey:@"startingPointX"];
        [trajectory setObject:[NSNumber numberWithInt:startingPoint.y] forKey:@"startingPointY"];
        
        endingPoint = CGPointMake(CGRectGetMaxX(self.frame), arc4random()%370 + 100);
        [trajectory setObject:[NSNumber numberWithInt:endingPoint.x] forKey:@"endingPointX"];
        [trajectory setObject:[NSNumber numberWithInt:endingPoint.y] forKey:@"endingPointY"];
    }
    
    
    return trajectory;
}

- (SKSpriteNode*)createBaddieWithTrajectory:(NSDictionary*)trajectory speedFactor:(int)speed{
    UIColor *badguyColor = [UIColor redColor];
    SKSpriteNode *badguy = [SKSpriteNode spriteNodeWithColor:badguyColor size:CGSizeMake(15, 15)];
    badguy.position = CGPointMake([[trajectory valueForKey:@"startingPointX"] intValue], [[trajectory valueForKey:@"startingPointY"] intValue]);
    badguy.name = @"badguy";
    
    SKAction *action = [SKAction rotateByAngle:M_PI duration:1];
    [badguy runAction:[SKAction repeatActionForever:action]];
    
    SKAction *moveAction = [SKAction moveTo:CGPointMake([[trajectory valueForKey:@"endingPointX"] intValue], [[trajectory valueForKey:@"endingPointY"] intValue]) duration:1.5*speed];
    [badguy runAction:moveAction completion:^{
        [self removeNode:badguy];
        [self enterBadguys];
    }];
    
    badguy.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:badguy.size];
    badguy.physicsBody.categoryBitMask = baddieCategory;
    badguy.physicsBody.collisionBitMask = laserCategory | particleCategory;
    badguy.physicsBody.contactTestBitMask = laserCategory | particleCategory;
    return badguy;
}

-(SKSpriteNode*)weaponForBadguy:(SKSpriteNode*)badguy{
    SKSpriteNode *weapon = [SKSpriteNode spriteNodeWithColor:[UIColor redColor] size:CGSizeMake(3, 3)];
    weapon.position = badguy.position;
    weapon.name = @"weapon";
    SKAction *atShooter = [SKAction moveTo:CGPointMake(shooter.position.x, shooter.position.y+10) duration:2];
    weapon.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:weapon.size];
    weapon.physicsBody.categoryBitMask = weaponCategory;
    weapon.physicsBody.collisionBitMask = shooterCategory;
    weapon.physicsBody.contactTestBitMask = shooterCategory;
    [weapon runAction:atShooter completion:^{
        [self removeNode:weapon];
    }];
    return weapon;
}

-(void)newBadguy{
    SKSpriteNode *newBadGuy;
    newBadGuy = [self createBaddieWithTrajectory:[self getBadguyTrajectory] speedFactor:1+score/1000];
    [badGuys addObject:newBadGuy];
    [self addChild:newBadGuy];
    [self addChild:[self weaponForBadguy:newBadGuy]];
    [Flurry logEvent:@"Created Badguy"];
}

-(void)removeNode:(SKNode*)node{
    [node removeFromParent];
}

#pragma mark Shooter

- (void)shootLaserAtLocation:(CGPoint)location{
    SKSpriteNode *laser = [SKSpriteNode spriteNodeWithColor:[UIColor greenColor] size:CGSizeMake(4, 50)];
    laser.position = CGPointMake(shooter.position.x, shooter.position.y);
    laser.name = @"laser";
    
    SKAction *shoot = [SKAction moveTo:CGPointMake(shooter.position.x, CGRectGetMaxY(self.frame)) duration:0.4];
    [laser runAction:shoot completion:^{
        [self removeNode:laser];
    }];
    
    laser.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:laser.size];
    laser.physicsBody.categoryBitMask = laserCategory;
    laser.physicsBody.collisionBitMask = baddieCategory;
    laser.physicsBody.contactTestBitMask = baddieCategory;
    
    [lasers addObject:laser];
    [self addChild:laser];
    
    [Flurry logEvent:@"Fired Laser"];
}

-(void)createPanGesture{
    if (!self.pan) {
        self.pan = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(dragShooter:)];
        self.pan.minimumNumberOfTouches = 1;
        self.pan.delegate = self;
        [self.view addGestureRecognizer:self.pan];
    }
}

-(void)createTapGesture{
    if (!self.tap) {
        self.tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapped:)];
        self.tap.delegate = self;
        [self.view addGestureRecognizer:self.tap];
    }
}

-(void)shootLaser: (UITapGestureRecognizer *)gesture {
    [self shootLaserAtLocation:[gesture locationInView:self.view]];
}

-(void)dragShooter: (UIPanGestureRecognizer *)gesture {
    CGPoint trans = [gesture translationInView:self.view];
    if (shooter.position.x < 0 && shooter.position.x > 320) {
        return;
    }
    SKAction *moveAction =  [SKAction moveByX:trans.x y:0  duration:0];
    [shooter runAction:moveAction];
    [gesture setTranslation:CGPointMake(0, 0) inView:self.view];
}

#pragma mark Touches Delegate

-(void)tapped: (UITapGestureRecognizer *)gesture {
    if (gameEnded) {
        return;
    }
    if (!gameInProgress) {
        [self beginGame];
        return;
    }
    
    if (count != 0) {
        return;
    }
    
    [self shootLaserAtLocation:[gesture locationInView:self.view]];
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (inCountdown) {
        return;
    }
    if (gameEnded) {
        return;
    }
    if (!gameInProgress) {
        [self beginGame];
        return;
    }
    
    if (count != 0) {
        return;
    }
    
    //Called when a touch begins
    
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        [self shootLaserAtLocation:location];
    }
 
}

#pragma mark Bonuses

-(void)comboBonus:(int)combo{
    [Flurry logEvent:@"Got Combo" withParameters:@{@"Combo Size" : [NSNumber numberWithInt:combo]}];
    if (combo >= 2 && combo < 10) {
        if (combo == 2) {
            [self displayLabelWithTitle:@"Combo Bonus!!"];
        }
        score = score + 100*combo;
        [self updateScoreWith:100*combo];
    } else {
        if (combo == 10) {
            [self displayLabelWithTitle:@"Huge Combo Bonus!!"];
        }
        score = score + 300*combo;
        [self updateScoreWith:300*combo];
    }
}


#pragma mark SKView Lifecycle Delegate

-(void)didMoveToView:(SKView *)view{
    [self createPanGesture];
    //[self createTapGesture];
}

-(void)willMoveFromView:(SKView *)view{
    gameInProgress = NO;
    gameEnded = NO;
    inCountdown = NO;
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    bg1.position = CGPointMake(bg1.position.x, bg1.position.y-1);
    bg2.position = CGPointMake(bg2.position.x, bg2.position.y-1);
    
    if (bg1.position.y < -bg1.size.height){
        bg1.position = CGPointMake(bg2.position.x, bg1.position.y + bg2.size.height*2);
    }
    
    if (bg2.position.y < -bg2.size.height) {
        bg2.position = CGPointMake(bg1.position.x, bg2.position.y + bg1.size.height*2);
    }
}

#pragma mark Collision Delegate
- (void)didBeginContact:(SKPhysicsContact *)contact{
    if (!gameInProgress) {
        return;
        
    }
    if (([contact.bodyA.node.name isEqualToString:@"shooter"] && [contact.bodyB.node.name isEqualToString:@"weapon"]) ||
        ([contact.bodyA.node.name isEqualToString:@"weapon"] && [contact.bodyB.node.name isEqualToString:@"shooter"]) ) {
        [self explosionAtLocation:shooter.position withSpread:NO];
        [shooter removeFromParent];
        [self gameOver];
        return;
    }
    
    if (([contact.bodyA.node.name isEqualToString:@"badguy"] && [contact.bodyB.node.name isEqualToString:@"laser"]) ||
        ([contact.bodyA.node.name isEqualToString:@"laser"] && [contact.bodyB.node.name isEqualToString:@"badguy"]) ) {
        [self explosionAtLocation:contact.bodyA.node.position withSpread:YES];
        [contact.bodyA.node removeFromParent];
        [contact.bodyB.node removeFromParent];
        [lasers removeObject:contact.bodyB.node];
        [badGuys removeObject:contact.bodyA.node];
        combo = 0;
        NSLog(@"Combo: %i", combo);
        [self newBadguy];
        [self calculateScoreUpdate];
        return;
    }
    
    if (([contact.bodyA.node.name isEqualToString:@"badguy"] && [contact.bodyB.node.name isEqualToString:@"particle"]) ||
        ([contact.bodyA.node.name isEqualToString:@"particle"] && [contact.bodyB.node.name isEqualToString:@"badguy"]) ) {
        [self explosionAtLocation:contact.bodyA.node.position withSpread:YES];
        [contact.bodyA.node removeFromParent];
        [contact.bodyB.node removeFromParent];
        [particleArrays removeObject:contact.bodyB.node];
        [badGuys removeObject:contact.bodyA.node];
        [self newBadguy];
        combo++;
        if (combo >= 2) {
            [self comboBonus:combo];
        }
        NSLog(@"Combo: %i", combo);
        [self calculateScoreUpdate];
        return;
    }
    
    if (([contact.bodyA.node.name isEqualToString:@"laser"] && [contact.bodyB.node.name isEqualToString:@"weapon"]) ||
        ([contact.bodyA.node.name isEqualToString:@"weapon"] && [contact.bodyB.node.name isEqualToString:@"laser"]) ) {
        [contact.bodyA.node removeFromParent];
        [contact.bodyB.node removeFromParent];
        [particleArrays removeObject:contact.bodyB.node];
        [lasers removeObject:contact.bodyA.node];
        return;
    }
}

- (void)didEndContact:(SKPhysicsContact *)contact{
    
}

@end
