//
//  Constants.h
//  Chain
//
//  Created by Russ Fellman on 3/19/14.
//  Copyright (c) 2014 Russ Fellman. All rights reserved.
//

#define ShooterFilename @"spaceship_chain"
#define BackDropFilename @"backdrop"
#define ExplosionFilename @"explosion"
#define BadGuyFilename @"egg_stripes"
#define ParticleFilename @"bird"
#define GameOverMessage @"GAME OVER!"
#define WelcomeMessage @"Start"
#define DefaultFontName @"Chalkduster"
#define FontColor @""
#define ApplicationName @"Captain Combo"

#define FlurryAppId @"V6V574838WY7RHWZ5DDC"
#define iTunesLink @"https://itunes.apple.com/us/app/captain-combo/id847197923?ls=1&mt=8"


