//
//  ViewController.h
//  Chain
//

//  Copyright (c) 2014 Russ Fellman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>
#import "MyScene.h"
#import "GameOverViewController.h"
#import <iAd/iAd.h>
#import "Flurry.h"

@interface ViewController : UIViewController<GameDelegate, UIAlertViewDelegate, GameOverViewControllerDelegate, ADBannerViewDelegate>

@property (nonatomic, retain) SKView *skView;
@property (nonatomic, retain) SKScene *scene;
@property (nonatomic, retain) GameOverViewController *govc;

@property (nonatomic, assign) BOOL comboBonus;

@end
