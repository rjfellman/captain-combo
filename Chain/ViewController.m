//
//  ViewController.m
//  Chain
//
//  Created by Russ Fellman on 3/17/14.
//  Copyright (c) 2014 Russ Fellman. All rights reserved.
//

#import "ViewController.h"
#import "GCHelper.h"

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Configure the view.
    self.skView = (SKView *)self.originalContentView;
    //self.skView.showsFPS = YES;
    //self.skView.showsNodeCount = YES;
    
    // Create and configure the scene.
    self.scene = [[MyScene alloc] initWithSize:self.skView.bounds.size delegate:self];
    self.scene.scaleMode = SKSceneScaleModeAspectFill;
    
    // Present the scene.
    [self.skView presentScene:self.scene];
    
    self.canDisplayBannerAds = YES;
}

#pragma mark GameCenter


- (BOOL)canDisplayBannerAds{
    return YES;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark GameDelegate

-(void)gameEndedWithScore:(int)score{
    //report score
    [[GCHelper sharedInstance] reportScore:score forLeaderboardID:@"CCSCORE99"];
    
    if (score > 10000) {
        [Flurry logEvent:@"HighScore!" withParameters:@{@"high_score": [NSNumber numberWithInt:score]}];
    }
    self.govc = [[GameOverViewController alloc] initWithNibName:@"GameOverView" bundle:nil score:score delegate:self];
    [self.view addSubview:self.govc.view];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [Flurry logEvent:@"lost game using iPad"];
         [self.govc.view setFrame:CGRectMake(100, 100, self.govc.view.frame.size.width*2, self.govc.view.frame.size.height*2)];
    } else {
         [self.govc.view setFrame:CGRectMake(20, 50, self.govc.view.frame.size.width, self.govc.view.frame.size.height)];
        [Flurry logEvent:@"lost game using iPhone"];
    }
    [self willMoveToParentViewController:self.govc];
}

-(void)comboBonusAchievment{
    self.comboBonus = YES;
}

#pragma mark GameOverViewController Delegate

-(void)willPlayAgain{
    [self.govc.view removeFromSuperview];
    [self.govc removeFromParentViewController];
    self.scene = nil;
    [self.skView presentScene:nil];
    
    // Create and configure the scene.
    self.scene = [[MyScene alloc] initWithSize:self.skView.bounds.size delegate:self];
    self.scene.scaleMode = SKSceneScaleModeAspectFill;
    
    // Present the scene.
    [self.skView presentScene:self.scene];
}

@end
