//
//  Badguy.m
//  Chain
//
//  Created by Russ Fellman on 3/20/14.
//  Copyright (c) 2014 Russ Fellman. All rights reserved.
//

#import "Badguy.h"

@implementation Badguy

-(id)initWithLevel:(int)level{
    self = [super init];
    if (self) {
        self = [self createBaddieWithTrajectory:[self getBadguyTrajectory] completion:^{
            NSLog(@"Baddie Reached end");
        }];
    }
    return self;
}

- (NSDictionary*)getBadguyTrajectory{
    NSMutableDictionary *trajectory = [[NSMutableDictionary alloc] init];
    CGPoint startingPoint;
    CGPoint endingPoint;
    //come in from the left or right
    if (arc4random() %2 + 1 == 2) {
        startingPoint = CGPointMake(CGRectGetMaxX(self.frame), arc4random() %400 + 100);
        [trajectory setObject:[NSNumber numberWithInt:startingPoint.x] forKey:@"startingPointX"];
        [trajectory setObject:[NSNumber numberWithInt:startingPoint.y] forKey:@"startingPointY"];
        
        endingPoint = CGPointMake(CGRectGetMinX(self.frame), arc4random()%400 + 100);
        [trajectory setObject:[NSNumber numberWithInt:endingPoint.x] forKey:@"endingPointX"];
        [trajectory setObject:[NSNumber numberWithInt:endingPoint.y] forKey:@"endingPointY"];
    } else {
        startingPoint = CGPointMake(CGRectGetMinX(self.frame), arc4random() %400 + 100);
        [trajectory setObject:[NSNumber numberWithInt:startingPoint.x] forKey:@"startingPointX"];
        [trajectory setObject:[NSNumber numberWithInt:startingPoint.y] forKey:@"startingPointY"];
        
        endingPoint = CGPointMake(CGRectGetMaxX(self.frame), arc4random()%400 + 100);
        [trajectory setObject:[NSNumber numberWithInt:endingPoint.x] forKey:@"endingPointX"];
        [trajectory setObject:[NSNumber numberWithInt:endingPoint.y] forKey:@"endingPointY"];
    }
    
    
    return trajectory;
}

- (SKSpriteNode*)createBaddieWithTrajectory:(NSDictionary*)trajectory completion:(void (^) (void))completion{
    SKSpriteNode *badguy = [SKSpriteNode spriteNodeWithColor:[UIColor redColor] size:CGSizeMake(20, 20)];
    badguy.position = CGPointMake([[trajectory valueForKey:@"startingPointX"] intValue], [[trajectory valueForKey:@"startingPointY"] intValue]);
    badguy.name = @"badguy";
    
    SKAction *action = [SKAction rotateByAngle:M_PI duration:1];
    [badguy runAction:[SKAction repeatActionForever:action]];
    
    SKAction *moveAction = [SKAction moveTo:CGPointMake([[trajectory valueForKey:@"endingPointX"] intValue], [[trajectory valueForKey:@"endingPointY"] intValue]) duration:1.5];
    [badguy runAction:moveAction completion:completion];
    
    badguy.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:badguy.size];
    badguy.physicsBody.categoryBitMask = baddieCategory;
    badguy.physicsBody.collisionBitMask = laserCategory;
    badguy.physicsBody.contactTestBitMask = laserCategory;
    return badguy;
}

@end
