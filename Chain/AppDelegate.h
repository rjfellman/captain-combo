//
//  AppDelegate.h
//  Chain
//
//  Created by Russ Fellman on 3/17/14.
//  Copyright (c) 2014 Russ Fellman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
