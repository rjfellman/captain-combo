//
//  Entity.h
//  Chain
//
//  Created by Russ Fellman on 3/20/14.
//  Copyright (c) 2014 Russ Fellman. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

static const uint32_t laserCategory = 0x1 << 0;
static const uint32_t baddieCategory = 0x1 << 1;
static const uint32_t particleCategory = 0x1 << 2;
static const uint32_t weaponCategory = 0x1 << 3;
static const uint32_t shooterCategory = 0x1 << 4;

@interface Entity : SKSpriteNode

@end
