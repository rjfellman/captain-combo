//
//  GameOverViewController.h
//  Chain
//
//  Created by Russ Fellman on 3/22/14.
//  Copyright (c) 2014 Russ Fellman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iAd/iAd.h>
#import "Flurry.h"
#import "UICountingLabel.h"
#import "GCHelper.h"

@protocol GameOverViewControllerDelegate <NSObject>

-(void)willPlayAgain;

@end

@interface GameOverViewController : UIViewController<ADBannerViewDelegate, GKGameCenterControllerDelegate>

@property (nonatomic, retain) IBOutlet UICountingLabel *scoreLabel;
@property (nonatomic, retain) IBOutlet UILabel *scoreDescLabel;
@property (nonatomic, retain) IBOutlet UIImageView *badgeImage;

@property (nonatomic, assign) int score;

@property (nonatomic, assign) id<GameOverViewControllerDelegate> delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil score:(int)score delegate:(id<GameOverViewControllerDelegate>)delegate;

-(IBAction)shareOnFacebook:(id)sender;
-(IBAction)shareOnTwitter:(id)sender;
-(IBAction)showLeaderboard:(id)sender;
@end
