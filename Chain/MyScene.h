//
//  MyScene.h
//  Chain
//

//  Copyright (c) 2014 Russ Fellman. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@protocol GameDelegate <NSObject>

-(void)gameEndedWithScore:(int)score;

@end

@interface MyScene : SKScene<SKPhysicsContactDelegate, UIGestureRecognizerDelegate>

-(id)initWithSize:(CGSize)size delegate:(id)delgate;

@property (nonatomic, retain) UIPanGestureRecognizer *pan;
@property (nonatomic, retain) UITapGestureRecognizer *tap;
@property (nonatomic, assign) id<GameDelegate> delegate;

@end
