//
//  Badguy.h
//  Chain
//
//  Created by Russ Fellman on 3/20/14.
//  Copyright (c) 2014 Russ Fellman. All rights reserved.
//

#import "Entity.h"

@interface Badguy : Entity

@property (nonatomic, assign) int level;

@end
